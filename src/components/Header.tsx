import styled from "styled-components"
import Link from "next/link"
import { color, spacing } from "/src/styleguide/index"
import MainStructure from "/src/components/MainStructure.tsx"

const LinkList = styled.ul`
    display: flex;
    padding-left: 0;
`
const StyledLink = styled.a`
    display: flex;
    padding: ${spacing.insetSquish("m")};
`

const Header = () => {
    return (
        <MainStructure as="nav" background={color.color("backgroundGreyLight")}>
            <LinkList>
                <li><Link href="/" passHref><StyledLink>Home</StyledLink></Link></li>
                <li><Link href="/wiki" passHref><StyledLink>Wiki</StyledLink></Link></li>
            </LinkList>
        </MainStructure>
    )
}

export default Header;