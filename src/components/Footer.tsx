import styled from "styled-components"
import { color } from "/src/styleguide/index"
import MainStructure from "/src/components/MainStructure.tsx"

const Footer = () => (
    <MainStructure as="footer" background={color.color("backgroundGreyLight")}>
        <footer>
            <p>Footer info</p>
        </footer>
    </MainStructure>
)

export default Footer