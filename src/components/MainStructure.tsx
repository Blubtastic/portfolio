import styled from "styled-components";
import { spacing } from "/src/styleguide/index";

const FullWidthContainer = styled.div<ContainerProps>`
    padding: 0 ${spacing.inset("m")};
    background: ${p => p.background && p.background};
`
const ContentContainer = styled.div`
    width: min(1170px, 100%);
    margin: auto;
`

type ContainerProps = {
    background?: string
}
type MainGridProps = {
    className?: string,
    children?: JSX.Element,
    as?: React.ElementType;
    background?: string;
}

const MainStructure = ({ className, children, as, background }: MainGridProps) => {
    return (
        <FullWidthContainer className={className} background={background}>
            <ContentContainer as={as}>
                {children}
            </ContentContainer>
        </FullWidthContainer>
    )
}
export default MainStructure
