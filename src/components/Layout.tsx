import styled from "styled-components"
import Header from "/src/components/Header.tsx"
import Footer from "/src/components/Footer.tsx"
import Base from "/src/styleguide/base.ts"
import Reset from "/src/styleguide/reset.ts"

const StyledLayout = styled.div`
    display: flex;
    flex-flow: column;
    min-height: 100%;
    > main {
        flex: 1;
    }
`

type LayoutProps = {
    children: JSX.Element
}

const Layout = ({ children }: LayoutProps) => {
    return (
        <StyledLayout>
            <Reset />
            <Base />

            <Header />
            <main>{children}</main>
            <Footer />

        </StyledLayout>
    )
}

export default Layout
