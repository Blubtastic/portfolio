import { createGlobalStyle } from "styled-components"
import { color, typography, rhythm } from "./index"

const Base = createGlobalStyle`
  html {
    overflow-y: scroll;
    scroll-behavior: smooth;
  }
  html, body{
    height: 100%;
  }
  #__next {
    height: 100%;
    display: flex;
    flex-flow: column;
  }
  body {
    font-family:'' "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: ${typography.size("base")};
    line-height: ${rhythm.rhythm("xl")};
    color: ${color.color("text")};
    background-color: ${color.color("background")};
  }
  h1, h2, h3, h4, h5 {
    color: ${color.color("text")};
    line-height: ${rhythm.rhythm("l")};
    margin: 0;
    font-weight: 600;
    margin: 0;
  }
  h4, h5 {
    font-weight: normal;
  }
  h1 { font-size: ${typography.size("heading1")}; }
  h2 { font-size: ${typography.size("heading2")}; }
  h3 { font-size: ${typography.size("heading3")}; }
  h4 { font-size: ${typography.size("heading4")}; }
  h5 { font-size: ${typography.size("heading5")}; }

  p {
    margin: 0;
    overflow-wrap: anywhere;
  }
  label {
    color: ${color.color("primary2")};
    font-weight: 500;
  }
  ul{
    margin: 0;
    padding: 0;
  }
  li{
    list-style-type: none;
  }
`

export default Base
