/**
 * Space defining units.
 * Inspired by https://medium.com/eightshapes-llc/space-in-design-systems-188bcbae0d62
 */

 const baseline = 1
 const multipliers = {
   xs: 0.25,
   s: 0.5,
   m: 1,
   l: 2,
   xl: 4,
 }
 /** @typedef { (size?: keyof typeof multipliers) => string } Spacing */
 /** @typedef { keyof typeof multipliers } Multipliers */
 /** @typedef { (size?: Exclude<Multipliers, "xs">) => string } SpacingLesser */
 const rhythm = (multiplier) => baseline * multipliers[multiplier]
 const unitify = (value, unit = "rem") => `${value}${unit}`
 
 // margin-top
 /** @type {Spacing} */
 export const stack = (size = "m") => unitify(rhythm(size))
 
 // margin-left
 /** @type {Spacing} */
 export const inline = (size = "m") => unitify(rhythm(size))
 
 // padding
 /** @type {Spacing} */
 export const inset = (size = "m") => unitify(rhythm(size))
 
 const lesser = (value = "m") => {
   const keys = Object.keys(multipliers)
   const currentIndex = keys.indexOf(value)
   const newIndex = keys[currentIndex - 1]
   if (currentIndex > 0) return multipliers[newIndex]
   throw new Error("Invalid size given")
 }
 const insetLesser = (value) => unitify(lesser(value))
 
 // Applies small top & bottom padding, large left & right padding
 /** @type {SpacingLesser} */
 export const insetSquish = (value = "m") => `${insetLesser(value)} ${inset(value)}`
 
 // Applies large top & bottom padding, small left & right padding
 /** @type {SpacingLesser} */
 export const insetStretch = (value = "m") => `${inset(value)} ${insetLesser(value)}`
 