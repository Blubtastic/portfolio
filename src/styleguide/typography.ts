/**
 * Font size units
 * Based on the Major Third type scale (1.250) on https://type-scale.com/?size=16&scale=1.125&font=Red%20Hat%20Display
 */

 const units = {
    xs: 0.75, // 12px
    s: 0.875, // 14px
    m: 1, // 16px
    l: 1.125, // 18px
    xl: 1.375, // 22px
    "2xl": 1.75, // 28px
    "3xl": 2.25, // 36px
    "4xl": 2.875, // 46px
    "5xl": 3.5 // 56px
  }
  
  const unit = (size = "m") => `${units[size]}rem`
  
  const sizes = {
    tertiary: unit("xs"),
    secondary: unit("s"),
    base: unit(),
    subLead: unit("l"),
    lead: unit("xl"),
    heading5: unit("l"),
    heading4: unit("xl"),
    heading3: unit("2xl"),
    heading2: unit("3xl"),
    heading1: unit("4xl"),
  }
  
  /** @type {(value?: keyof typeof sizes) => string} */
  export const size = (value = "base") => sizes[value]
  