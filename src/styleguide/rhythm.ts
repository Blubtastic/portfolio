const baseline = 1;
const multipliers = {
  "2xs": 0.25,
  xs: 0.5,
  s: 0.75,
  m: 1,
  l: 1.25,
  xl: 1.5,
  "2xl": 1.75,
  "3xl": 2,
  "4xl": 2.25,
}

/** @type { (multiplier?: keyof typeof multipliers) => string } */
export const rhythm = (multiplier = "m") => baseline * multipliers[multiplier]
