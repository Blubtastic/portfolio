export const colorPalette = {
    gray: {
      100: "hsl(204, 45%, 98%)",
      200: "hsl(210, 38%, 95%)",
      300: "hsl(214, 32%, 91%)",
      400: "hsl(211, 25%, 84%)",
      500: "hsl(214, 20%, 69%)",
      600: "hsl(216, 15%, 52%)",
      700: "hsl(218, 17%, 35%)",
      800: "hsl(218, 23%, 23%)",
      900: "hsl(220, 26%, 14%)",
    },
  } as const;
  
  export type Palette = typeof colorPalette;
  export type Hue = keyof Palette;
  
  /**
   * Retrieve a color from the color palette
   *
   * @param hue The hue of the desired color
   * @param tone The tone of the desired color. The higher the number, the darker the color.
   */
  export const palette = <T extends Hue, U extends keyof Palette[T]>(hue: T, tone: U): Palette[T][U] =>
    colorPalette[hue][tone];
  
  /*
  We use https://mdigi.tools/darken-color/ to create dark and light color variants for things like
  hovering/pressing a button. Make the difference noticeable, but stay close to the original color.
  */
  export const colors = {
    /* SIT-farger */
    primary1: "#102C40" /* Bjørnebær */,
    primary1Light: "#163d58",
    primary1Dark: "#06121a",
    primary2: "#F9F8F0" /* Hylleblomst */,
    primary2Light: "#fbfaf4",
    primary2Dark: "#f6f5e9",
    mainSecondary: "#6AD3B4" /* Avokado (variant 2) */,
    mainSecondaryLight: "#8fdec7",
    mainSecondaryDark: "#3ac49b",
  
    /* Ekstra farger */
    grey: "#E7E5E3",
    greyLight: "#F0F0F0",
    greyDark: "#d3d3d3",
    link: "#0052cc",
    keyboardFocus: "#0052cc",
    border: "#75777B",
    success: "#008038",
    error: "#CA3700",
    textWhite: "#fff",
    textGray: "#5A5A5A",
    textBlack: "#000508",
    textBlue: "#102C40",
    backgroundBlack: "#000508",
    backgroundGreyLight: "#F0EFEF",
    backgroundWhite: "#ffffff",
  
    cardAvailable: "#F0EFEF",
    cardAvailableHover: "#EAEAEA",
    cardUnavailable: "#E4E4E4",
    cardUnavailableHover: "#D6D6D6",
    background: "#fff",
    text: "#070707",

  } as const;
  
  export type Color = keyof typeof colors;
  
  /**
   * Retrieve a color from the semantic color palette
   *
   * NB! Only use this function when the semantic name fits with your usage needs.
   *
   * @param name The semantic name of the desired color
   */
  export const color = (name: keyof typeof colors) => colors[name];
  