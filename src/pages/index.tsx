import styled from "styled-components"
import { spacing } from "/src/styleguide/index"
import type { NextPage } from 'next'
import Head from 'next/head'
import MainStructure from "/src/components/MainStructure.tsx"

const StyledMain = styled(MainStructure)`
  margin-top: ${spacing.stack("xl")};
`

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Hjem - martinstoro</title>
        <meta name="description" content="My personal portfolio" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <StyledMain>
        <h1>Welcome!</h1>
        <p>Homepage coming soon ...</p>
      </StyledMain>
    </>
  )
}

export default Home
