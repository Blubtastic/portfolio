import ReactMarkdown from 'react-markdown'
import wikiPage from "../../wiki/UU_10_prinsipper.md"
import MainStructure from "/src/components/MainStructure.tsx"

const Wiki = () => {
    return (
        <MainStructure>
            <ReactMarkdown>{wikiPage}</ReactMarkdown>
        </MainStructure>
        )
}

export default Wiki