# Kartløsninger

De fleste bruker google maps som kartløsning i applikasjonen sin. I noen tilfeller er det et bra valg, men det velges ofte bare fordi alle kjenner til det. 

Google Maps er ofte dyrere og lite tilpasset bruksområdet. Det finnes mange alternativer som er både billigere, mer fleksible og hindrer at man blir låst til en leverandør. Ved å bruke en kartpakke i frontend i kombinasjon med en tiling-server for å levere _innholdet_ i kartet kan man lage en mye bedre tilpasset løsning som også er billigere i lengden. 

Her er et eksempel på en teknologikombinasjon som kan funke: 
- **Frontendpakke**: [Pigeon Maps](https://www.npmjs.com/package/pigeon-maps)
  - _Enkel kartimplementasjon uten bloat._
- **Tiling-server**: [Maptiler](https://www.maptiler.com/cloud/)
  - _Enkel karttjeneste uten unødvendige funksjoner som øker kostnaden._

Maptiler gir deg tilgang til å modifisere hva som skal være synlig på kartet (f.eks busstopp og campus), og vil ikke vise kommersielle reklamer på lik linje som Google Maps vil gjøre _(hvor butikker kan betale for å vises på Google Maps)._

I denne [prisoversikten over Maptilers tjenester](https://www.maptiler.com/cloud/estimate/.) kan man se hva det vil koste over tid, slik at man enkelt kan sammenligne med populære full-feature karttjenester som Mapbox og Google Maps. 
