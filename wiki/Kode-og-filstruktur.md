# Mappe- og filstruktur

Filer som er direkte relevante for _bygging_ av frontendapplikasjonen kan legges i  `src/` mappa. Filer som kan være knytta til ting utenfor frontendapplikasjonen legges utenfor, som .env filer, annen config, package manager stuff, utviklersetup etc.


## components/
I denne mappa ligger de fleste react-komponentene. 
Tommelfingerregelen er at ting som har mye med React å gjøre hører hjemme her, i motsetning til f.eks sider, services eller teknologispesifikke ting. Denne mappen kan ha undermapper om mange komponenter hører sammen, som f.eks inputkomponenter. Det kan være smart å skille _styling_ av komponenter fra _logikken_ til komponenter, så en mulighet er å lage en _input_ mappe med selve ustylede komponenter med logikk, og en _styled-input_ mappe hvor inputkomponentene importeres og blir stylet. 

En _Structure_ mappe kan være nyttig for komponenter som kun brukes for strukturering. Disse komponentene har lite eller ingen logikk, og tar ofte inn komponenter som children som de rendrer inni seg. Eksempler er modaler, tab-views, containers, etc..



## Filstruktur for .tsx-filer

Det er lite regler for hvordan en typisk reactfil skal struktureres, så her er et forslag på rekkefølgen av innholdet i en slik fil: 
1. imports
1. styled-components
1. stateless funksjoner _(uavhengig av komponentens tilstand)_
1. interfaces _(en typescript-ting. F.eks IProps)_
1. `export const ComponentName: React.FC<IProps> = ({ prop1, prop2 }) => (`
1. let/consts/hooks (i rekkefølgen):
	1. queries
	1. mutations
	1. state
	1. effects
1. Data-transform _(transformerer data fra typ props eller state)_
1. Handlers _(handles user input)_
1. `return <></>`