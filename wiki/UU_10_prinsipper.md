# Ti hovedprinsipper for universell utforming

Om man følger disse ti prinsippene for universell utforming vil man oppfylle de fleste kravene som er pålagt av staten for nettsider ment for bruk av befolkningen _(dvs. så å si alt annet enn internløsninger)_. 

Disse prinsippene er også til stor hjelp for å lage gode brukergrensesnitt generelt, så å bruke dem fra starten av i et prosjekt vil gjøre det helhetlige nivået til både kodebasen og sluttproduktet bedre. 

Her er en kort [video om universell utforming (7 min)](https://www.youtube.com/watch?v=3f31oufqFSM) som viser hvorfor dette er så viktig. 

Denne guiden er basert på UUtilsynet sin [guide for å sjekke din egen nettside](https://www.uutilsynet.no/english/checking-your-own-website/916). Den inneholder mer utdypende informasjon enn denne oversikten, så det anbefales å også sjekke ut denne om du skal jobbe med UU. 





## 1. Keyboard navigation
![Keyboard navigation](./.attachments/1keyboard.png)

Krav
1. Snarvei for "**Hopp til hovedinnhold**" øverst på siden
   - _Skal kun være synlig når den er markert med keyboard._
1. **Keyboardbrukere kan gjøre alt** en musbruker kan
   - _Å bruke kun keyboardnavigasjon skal ikke utelukke noe funksjonalitet_
1. **Logisk rekkefølge** på tab-ing
   - _Vanlige feil inkluderer sikksakkbevegelser mellom kolonner, tilhørende elementer gruppert feil, fokus som starter nede på siden etc.._
1. Tydelig **fokusmarkering**
   - _F.eks blå outline_
1. **Fokusbytte** når brukeren aktiverer endringer i grensesnittet
   - _Modal åpnes/lukkes, innhold/struktur endres, etc.._
   - _Flytter fokuset som forventet?_


## 2. Magnification & responsive design
![Magnification & responsive design](./.attachments/2responsive.png)

Krav
1. **Responsive design**
   - _Nettsiden skal se riktig ut og fungere bra på alle skjermstørrelser_
   - _>360px holder_
2. Nettsiden skal støtte **verktøy for å forstørre skjermen**
   - _Complement hover with click_
   - _Check your hover contrasts_
   - _Show results close to where it was triggered_
   - _Keep as much as possible to one column_   
   - _Guide for å støtte [tilgjengelighet for skjermforstørring](https://axesslab.com/make-site-accessible-screen-magnifiers/)_


## 3. Colors and contrast
![Colors and contrast](./.attachments/3color.png)

Krav
1. **Kontrast** mellom tekst og bakgrunn må være minst
   - _4.5:1 på vanlig tekst_
   - _3.5:1 på stor tekst (120-150% større enn paragraftekst)_
   - _[Info om fargekontrast](https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Perceivable/Color_contrast)_
2. **Farge** må ikke brukes som eneste virkemiddel
   - _Fargeblinde og svaksynte_
   - _Lav skjermkvalitet: lysstyrke, gjenskinn, feil innstilt, etc.._


## 4. Headings
![image.png](./.attachments/4headings.png)

Krav
1. Unngå å hoppe over headings (h1 -> h3)
   - _Ikke bra for SEO og screen readers, og kan forvirre bruker._
2. Bruk headings der det passer
   - _Alle headingstørrelser har sine bruksområder. Noen ganger kan `<h5>` eller `<h6>` være bedre egnet strukturellt, selv om de ligner på `<p>` i størrelse._
3. Bruk headings for å gjøre det lett for bruker å orientere seg

## 5. Links
![image.png](./.attachments/5links.png)

Krav
1. Lenker må være **markert** med mer enn kun farge
   - _F.eks understreking_
2. Lenker må **endre utseende** når bruker svever over den
   - _F.eks å fjerne underline_
3. Bruk **`<a>`** til hyperlink-navigering
4. Linkteksten skal **indikere hvor linken går**, selv uten kontekst.
   - _Dårlig: Learn more about our products <a href="/">here</a>_
   - _Bra: Learn more <a href="/">about our products</a>_
5. Eksterne linker og ikke-HTML-linker skal **spesifisere typen**
   - _Eksterne linker: <a target="_blank" href="https://www.wikipedia.org">Wikipedia (opens in new tab)</a>_
     - _Ikoner kan også brukes_
   - _Ikke-HTML-ressurser: <a href="2017-annual-report.ppt">2017 Annual Report (PowerPoint)</a>_
6. Lenkede bilder skal ha **alternativ tekst** som beskriver lenkens mål
   - _alt="facebook" holder ikke hvis målet er virksomhetens facebookside_

## 6. Images
![image.png](./.attachments/6images.png)

Krav
1. Alle bilder skal ha **alternativ tekst**
   - _alt=""_
2. Informasjonsbærende bilder skal ha **beskrivelse av motivet**
   - _alt="tre gamle traller"_
3. Lenkede bilder skal ha beskrivelse av **hvor linken leder**
   - _alt="boligsparing for unge"_
4. Dekorative bilder skal ha **tom alternativ tekst**
   - _alt=""_
   - _Også bilder som supplerer tekst, f.eks "Print ut 🖨️"_
5. Ikke representer **tekst som bilde**
   - _Om du må, skal teksten også representeres i form av tekst._

## 7. Forms
![image.png](./.attachments/7forms.png)

Krav
1. Skjemaelement skal ha en **synlig identifikasjon**
   -  _I form av ledetekst, instruksjon eller ikon/symbol/bilde_
2. **Identifikasjonen** skal være koblet til kontrollen programmatisk
   - _`<label>` skal være kobla til `<input>` (eller lignende) programmatisk._ 
3. Å **trykke på identifikasjonen** aktiverer tilhørende felt
   - _Linken mellom identifikasjonen og skjemaelement må derfor være korrekt_
4. **Obligatoriske** felter skal være markert
   - _Om et symbol brukes skal meningen med symbolet forklares før det tas i bruk første gang i skjemaet._
   - _F.eks være "Obligatoriske felter er merket med *"_
5. **Feil brukerinput** skal kommuniseres tydelig
6. **Finansielle og juridiske** skjema har ekstra regler

## 8. Search
![image.png](./.attachments/8search.png)

Krav
Dette punktet er beskrevet veldig flytende i UUtilsynets sider. Det viktigste er å sørge for at brukeren lett finner det hun leter etter. For mange er søkefunksjonen det mest brukte verktøyet for å finne informasjonen de søker, så det er viktig at dette gjøres riktig. 

Linken ovenfor beskriver noen viktige punkter for å lage god søkefunksjonalitet. 

## 9. Page titles
![image.png](./.attachments/9pagetitles.png)

**Krav**
1. **Alle sider** skal ha sidetittel
2. Sidetittel bør vise en **beskrivelse av sidens innhold**, og helst ende med nettsidens/organisasjonen navn
3. Sidetittel skal være på **brukerens valgte språk**

## 10. Code validation
![image.png](./.attachments/10codevalidation.png)

Krav
1. Gyldig **HTML**
   - Må være riktig nøstet
   - Må ha korrekte start- og avslutningstagger
   - Må ikke ha duplikate angivelser av samme attributt
2. **ID-er** (i HTML) aldri brukt flere ganger
3. Gyldig **CSS**
   - Må være så korrekt som mulig
   - Må ha minimal kode utenfor standarden
   - Må støttes av alle nettlesere som er relevant å bruke
     - _Husk at mobiler har egne versjoner av nettlesere, og mange bruke innebygde nettlesere som f.eks Samsung Internet Browser_


