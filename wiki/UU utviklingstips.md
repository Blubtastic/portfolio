# Utviklingstips
Denne siden forklarer metoder for å utvikle gode løsninger hvor UU er en del av løsningen fra starten av. Det som lages vil da være solid nok til at mange av UU-kravene blir oppfylt automatisk. 


## Hovedtanken
Å lage gode løsninger fra starten av, med fokus på at den skal fungere bra for _alle_. For å oppnå dette er det viktig å følge standarder, og å tenke på skalerbarhet og clean code. 

Det handler ikke om å legge til ekstra funksjonalitet for å støtte UU, men heller å lage solide løsninger med brukervennlighet og tilgjengelighet i fokus. 
Dette kan sammenlignes med å lage en kombinert trappegang som kan brukes av både gående og rullestolbrukere, i stedet for å bygge en frittstånde rullestolrampe ved siden av trappeoppgangen. Begge løsninger funker, men den siste løsningen er dobbelt så mye arbeid, blir mer kompleks, og tar mer plass. Så alle involverte - både utviklere, kunden og sluttbrukerne tjener på løsninger som er tilpasset alle. Det er så klart tilfeller hvor ting må tilpasses visse brukergrupper, men med riktig tankegang blir det sjeldent behov for spesialtilpasning.

Denne videoen om [Web Accessibility Perspectives (7 min)](https://www.youtube.com/watch?v=3f31oufqFSM) forklarer denne tankegangen bra.



## Semantisk HTML
Å skrive semantisk HTML går ut på å skrive kode med _tydelig definert innhold_. Man gjør dette ved å bruke riktige HTML-elementer basert på hva man ønsker å oppnå. Det finnes både semantiske og ikke-semantiske HTML-elementer. 
> _A semantic element clearly describes its meaning to both the browser and the developer._
- Eksempler på **ikke-semantiske** elementer: `<div>`og `<span>` - _Sier ingenting om innholdet_
- Eksempler på **semantiske** elementer: `<form>`, `<table>` og `<article>` - _Definerer tydelig innholdet._

Å skrive semantisk HTML gir forståelig kode samtidig som at nettlesere forstår innholdet uten at det må legges til ekstra kode som aria-labels og SEO-metadata. Det gir mye gratis funksjonalitet som bredere støtte blandt nettlesere, mange edge-cases funker out-of-the-box, siden din dukker opp i flere søkeresultater osv. 

Mozillas dokumentasjon er en god måte å få [oversikt over HTML-elementer](https://developer.mozilla.org/en-US/docs/Web/HTML/Element).

## Interaktive elementer
I prinsippet bør alt en bruker kan interagere med bør være et _interaktivt html-element_
  - Eksempler på interaktive elementer er `<a>`, `<button>`, `<input>`, `<select>`, `<textarea` og `<details>`.
  - _Unngå å bruke ikke-interaktive elementer som `<div>` på noe som skal interageres med._

I noen tilfeller er man nødt til å bruke f.eks `<div>` for å lage et eget input-felt ettersom mange standard HTML-elementer har begrensede tilpasningsmuligheter. Da er man nødt til å implementere alt av tilgjengelighetsstøtte selv, som vil si å legge på aria-tags, kode keyboardnavigasjon, osv osv.. Det er derfor viktig å være bevisst på hvor mye arbeid dette er før man bestemmer seg for å lage noe selv, og se på mulighetene for å bruke et komponentbibliotek som f.eks [react-aria](https://react-spectrum.adobe.com/react-aria/index.html) i stedet.


## Lenker og Knapper
Det kan være vanskelig å skille mellom når man bør bruke `<a>` og `<button>` for knapper. Du kan derimot komme langt ved å følge disse retningslinjene:
- Bruk lenker `<a>` for å navigere til en _ny URL_.
- Bruk knapper `<button>` til _alt annet_.

Retningslinjene ovenfor fører til en ny problemstilling: _når er det ønskelig å navigere til en ny URL?_ 
Det kan være hjelpsomt å stille seg spørsmålene nedenfor: 
1. Gir det mening å høyreklikke på elementet, og velge "Åpne i ny fane"?
2. Gir det mening å bokmerke den nye visningen?
3. Gir det mening å bruke tilbake-knappen i nettleseren for å navigere til forrige visning?

**Hvis du svarer "ja" på ett eller flere av spørsmålene, er det en sterk indikasjon på at du bør bruke lenke.**

_Hvorfor er dette viktig? En `<a>` med tom href="" vil skape uforventet oppførsel når bruker kopierer/drar lenker, åpner lenke i nytt vindu, bokmerker lenken, eller når javascript loader, feiler eller er skrudd av. Det er slitsomt for bruker og skaper forvirring._


## HTML-Struktur
**HTML-rekkefølgen bør være lik brukergrensesnittet**
- Om man følger dette vil **tab-index** være riktig fra starten av slik at rekkefølgen gir mening for keyboardbrukere. Det blir også lettere å støtte flere skjermstørrelser om ting må flyttes mindre rundt på. 
  - _I en mobile-first tankegang er HTML-strukturen lik mobilgrensesnittet. Dette gjør det lettere å lage varianter til ulike enheter, og applikasjonen vil generelt oppføre seg mer likt på ulike enheter._
- Dette gir også andre fordeler som at koden er **lesbar** og det er lett å **mentalt koble** koden til brukergrensesnittet.

Strukturen til HTML er viktig siden det er den faktiske strukturen til nettsiden. CSS kan endre rekkefølge og layout som brukeren _ser_, men dette påvirker IKKE det verktøy ser, som tastatur, skjermlesere og søkemotorer. En god HTML-struktur vil derfor gjøre at:
- Siden automatisk oppnår flere UU-krav
- Blir mer brukervennlig
- Blir mer robust og vedlikeholdbar
- Dukker opp i flere søk


## Responsiveness

> _"The application should support all screen sizes of 360px width and above. This means designing a fluid (responsive) page with components that can move around depending on available space. With a smart design, media queries are only needed when the mobile and web interfaces need different layouts, or certain elements need to be hidden."_