# Typografi
Tekst står for mesteparten av informasjonen på nettsider. Ha å en god forståelse av typografi er derfor en sentral del av å lage en brukervennlig nettside. 

Man kommer allerede langt ved å være bevisst over hvordan mennesker leser tekst. Noen viktige konsepter er: 
- Blikket faller først øverst til venstre
- Kontrast
  - Tekststørrelse - større tekst trekker mer oppmerksomhet
  - Teksttykkelse - tykkere tekst mer **oppmerksomhet**
  - _Andre virkemidler - italic, STORE BOKSTAVER, farger osv.._
- Bruk med omhu - overdreven bruk av virkemidler har motsatt effekt
  - _Om alt er viktig, er ingenting viktig._


## Relative enheter
Tekststørrelse bør defineres i _relative enheter_ (rem & em). Dette gjelder også det som bør skalere med _teksten_. Eksempler er linjehøyde, paragrafbredde og høyde mellom paragrafer. I noen tilfeller er det passende at f.eks bilder eller andre elementer også skalerer med tekststørrelse. 

Grunnen til dette er at det gjør løsningen mer fleksibel for endring, og bedre for brukere som må forstørre tekst for å bruke løsningen effektivt. De fleste moderne nettlesere har innebygde verktøy å endre tekststørrelsen. 

I tillegg bør den vertikale avstanden (margin) mellom to avsnitt være den samme som avsnittets linjehøyde (line-height) for å bidra til at designet får [vertikal rytme](https://zellwk.com/blog/why-vertical-rhythms/).
Relative enheter kan brukes til de aller fleste dimensjoner, inkludert [media queries](https://zellwk.com/blog/media-query-units/).

For å sette en global rem til 16px kan legge til denne css-regelen på rotnivå (html-elementet):
```
html { font-size: 100% } /* This means 16px by default*/
````

### Hva er rem og em
- **rem**: fontstørrelsen til rotelementet
- **em**: fontstørrelsen til det valgte elementet

Tommelfingerregel for å velge riktig
- Bruk **em** hvis elementet skal skalere med sin _egen_ fontstørrelse
- Bruk **rem** til alt annet.

Kodeeksempel
```
p {
  font-size: 1rem;
  line-height: 1.5rem;
  padding: 0.5em 1.5rem;
  max-width: 45rem;
}
@media (min-width: 48em) {
  html {
    font-size: 120%;
  }
}
```
Ressurs: [hvorfor vi bruker em & rem til tekstskalering](https://zellwk.com/blog/rem-vs-em/)



# Fargevalg
Det viktigste her er at man har en struktur som alle er enige i å følge. En applikasjon uten et ordentlig fargesystem vokser fort ut av kontroll og blir veldig vanskelig å vedlikeholde. 

Her er det viktig å ha et robust system med fargevariabler knyttet opp til en felles fargemal (ofte utviklet av designere), slik at man unngår at farger blir hardkodet inn i applikasjonen. Dette systemet kan utvikles i samspill med designere for å lage et system som er vedlikeholdbart, skalerbart, og lett å modifisere ettersom design ofte endres underveis. 

_Denne seksjonen vil fylles mer ut etterhvert som jeg lærer mer om fargesystemer._



# Tastaturnavigasjon
Støtte for tastaturnavigasjon krever blandt annet tydelig fokusmarkering. Koden nedenfor legger til dette _kun_ på keyboardmarkering, slik at vanlige interaksjoner med mus ikke krasjer med designet til siden. Fargen på fokusmarkeringen kan endres for å bedre matche design. 

Elementer markert med keyboard får en egen visuell markering. Det kan være greit å skille mellom hvordan vanlige elementer (typ knapper og linker) markeres vs mer komplekse inputelementer markeres (typ felter man skriver i) slik at det er lett å forstå hvordan de skal interageres med. Det kan derfor være greit å markere: 
  - Input-elementer som krever _keyboardinput_ med **border**
  - Resten med **outline**
    - _NB: Ta hensyn til eksisterende borders/outlines så ikke elementer ikke "hopper" når markert._

Dette kan eksperimenteres med for å finne en passende løsning ettersom det er flere alternativer som funker. 

_Her er eksempelkode på hvordan dette kan løses:_
```
/* input-elementer */
input, textarea, select, optgroup {
    line-height: ${rhythm.rhythm()};
    &:focus {
      border: 1px solid ${color.color("keyboardFocus")};
      outline: 1px solid ${color.color("keyboardFocus")};
    }
    &:focus-visible {
      border: 1px solid ${color.color("keyboardFocus")} !important;
      outline: 1px solid ${color.color("keyboardFocus")} !important;
    }
  }

/* vanlige elementer */
summary, iframe, area{
  &:focus-visible {
    outline: 2px solid ${color.color("keyboardFocus")} !important;
  }
}
button
{
  &:focus-visible {
    outline: 2px solid ${color.color("keyboardFocus")} !important;
  }
}
a {
  color: ${color.color("link")};
  text-decoration: underline;
  &:focus-visible {
    outline: 2px solid ${color.color("keyboardFocus")} !important;
  }
}
`;
```
_Koden har fortsatt litt quirks og har forbedringspotensiale, som å fjerne !important og å gjøre koden mer kompakt. Dette vil bli oppdatert i framtiden._



# Grid og Flex

Grid og flex er egenskaper i CSS som brukes for å bygge den visuelle strukturen til HTML-komponenter. De kan begge sammenlignes med bootstrap sin grid-struktur, og brukes for å strukturere siden ved å plassere elementer i rutenett (grid) eller fleksible (flex) rader eller kolonner. 

Både grid og flex har eksistert lenge nok til at de er trygge å bruke i nettsideutvikling, og har blitt viktige verktøy som enhver frontendutvikler bør ha kunnskap om. De bør derimot ikke brukes til alt, så å vite når man skal bruke det krever erfaring og eksperimentering.

Noen tips som kan hjelpe for å velge:
- Trenger du en _endimensjonal_ layout? **Flex** er nok riktig.
- Trenger du en _todimensjonal_ layout? **Grid** er nok riktig. 

Her er et par nyttige ressurser for å forstå grid og flex: 
- [En komplett guide for CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [En komplett guide for CSS Flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)


## Gap
Tidligere var man avhengig av margin for å lage mellomrom mellom komponenter, som ofte skapte rot og kompleksitet i layouten. Gap er en egenskap som kan brukes på elementer definert med ``display: grid`` eller ``display: flex`` for å definere _mellomromet_ mellom alle direkte underkomponenter _(direct children)_ av denne komponenten.

Dette betyr at man kan fjerne margin på alle elementer i en grid/flex struktur, og heller bruke gap for å definere mellomrommet til dem. Fordelen er at alt blir definert på _ett_ sted, og at gap ikke legger til mellomrom på elementene som ligger på _kanten_ av containeren. Man slipper da å deale med å fjerne margin på visse elementer for å sørge for at kantene på containeren er samjustert med resten av siden. 

Margin er fortsatt et veldig viktig verktøy, men med grid og flex kan man heller bruke gap i tilfeller hvor margin ikke er en ideell løsning. 

## Mobile first
Det kan ofte være en god ide å designe løsningen som som "mobile-first", som betyr at man først lager en en-kolonne-struktur slik at det fungerer på mobil, og så legger til varianter med flere kolonner for større skjermer som desktop og pads. 

### Fordeler
Fordelen med dette er at løsningen blir responsiv fra starten av, og man har alltid en _fallback_ i at en-kolonne-strukturen funker på alle enheter. Det er så klart ikke helt ideellt å ha en en-kolonne-struktur på desktop, men det fungerer overraskende bra og er uansett bare ment som en fallback om brukeren f.eks bruker internet explorer eller et eller annet tull. En annen veldig viktig fordel er at man kan da lage HTML-strukturen så å si identisk til det faktiske utseendet til mobilversjonen, siden det kun er en lang kolonne. HTML-elementer kan da være lagt i samme rekkefølge i koden som det vises på skjermen, som er en enorm fordel både for kodelesbarhet og universell utforming. 

### Implementasjon
En annen positiv ting med mobile-first er at man kan bruke grid og flex til å implementere løsningen enkelt. Tanken er at mobilvisningen lages vha. flex hvor man har èn stor kolonne definert med ``display: flex``. Når skjermstørrelsen går over en viss grense, kan man bytte ut denne strukturen med ``display: grid``, og flytte elementer over i flere kolonner. Dette beholder den lettleselige kolonnestrukturen i HTML-koden, samtidig som at den er fleksibel og kan (visuellt) flyttes rundt på større skjermer. 

Det er viktig å huske at når man flytter HTML-elementer visuelt vha ``display: grid``, så endres ikke den _logiske_ rekkefølgen. Dette er viktig fordi verktøy som keyboardnavigering, skjermlesere ol. følger strukturen i HTML-koden. Sørg derfor for at den logiske rekkefølgen fortsatt gir mening når man flytter på elementer visuelt. Dette kan enkelt testes ved å tabbe gjennom siden med keyboard, og se om det neste elementet som velges er det man forventer blir valgt. 
