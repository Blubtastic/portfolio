# Nyttige teknologier
- [Typescript](https://www.typescriptlang.org/): _Sterkt typet programmeringsspråk som er en utvidelse av javascript. Gjør utvikling mer feilfritt og strømlinjet på bekostning av at det er mer å lære._
- [Formik](https://formik.org/): _Skjemabibliotek som tar seg av med skjemalogikk_
- [i18next](https://www.i18next.com/): _Implementasjon av flerspråkstøtte_
- [styled-components](https://styled-components.com/): _Knytter styling til komponenter_
- [Storybook](https://storybook.js.org/): _Rammeverk for å dokumentere og samkjøre design mellom utviklere og designere. Ble tatt lite i bruk pga mangel på designer._


## Nyttig for struktur
- [Babel](https://babeljs.io/): Javascript compiler. Kompilerer moderne js til browser-støttet js
- [Webpack](https://webpack.js.org/): Javascript module bundler. Tar moduler med avhengigheter og genererer statiske filer av dem.
- [eslint](https://eslint.org/): Linter for javasscript. Analyserer kode og finner problemer. Kan sette regler/krav for godkjent kode.
- [commitlint](https://commitlint.js.org/#/): Linting av commit-meldinger. For å sette og opprettholde en standard i teamet.
- [Husky](https://typicode.github.io/husky/#/): Utfører operasjoner ved commit/push, som å kjøre tester, linting, etc
  - _Modern native Git hooks made easy_
- [node.js](https://nodejs.org/en/): Kjør javascript utenfor nettlesere.
- [yarn (berry)](https://yarnpkg.com/getting-started/install): Package manager, alternative to npm.


_Notat: for å se alle pakker i prosjektet med nåværende -vs- siste versjon, kjør `yarn upgrade-interactive`_