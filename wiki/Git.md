# Git 

## Navngiving av branches
Når man navngir nye branches i prosjektet er det fint om man navngir de på et spesifikt format for å gjøre det lettere å organisere branches. Følgende format blir brukt:

`fornavnX/issuenumber-some-description`

hvor X er første bokstav i etternavn. Eksempel på et branch-navn som følger dette formatet:

`erlends/5004-contact-form-categories` og `erlends/4999-slideshow-crashes-without-images`


## Commits
Conventional commits er nyttig ettersom det gir struktur og lesbarhet til commits. 
https://www.conventionalcommits.org/en/v1.0.0/


## Ressurser
Det er lett å fucke opp med git, og å fikse fuckupen kan være så å si umulig om man ikke er en git-guru. Å finne løsninger på google krever ofte så mye forståelse av git at man ender opp mer forvirra og frustrert enn før man leste det. 

Denne ressursen kan derfor hjelpe ved at den svarer på typiske git fuck-ups på en enkel, forståelig og humoristisk måte: https://ohshitgit.com/