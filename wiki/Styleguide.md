# Typography
The typography is based on the [Major Second type scale](https://type-scale.com/?size=16&scale=1.250&font=Red%20Hat%Display) (1.250). 

```jsx
import { typography } from "styleguide/typography";

const value = typography("xl");
```

# Spacing
Different ways of setting paddings, margins, border thickness and sizes. 

## Stack
(aka. margin-top)

Why we only use [single direction margins](https://csswizardry.com/2012/06/single-direction-margin-declarations/) (margin-top)

Why we use [margin-top insted of margin-bottom](https://www.nine.com.au/tech/2018/05/02/08/54/css-margin-top-vs-bottom)

```jsx
import { spacing } from "styleguide/spacing";

const value = spacing("xl");
```

## Inline
(aka. margin-left)

```jsx
import { spacing } from "styleguide/spacing";

const value = spacing.inline("xl");
```

## Inset
(aka. padding)
```jsx
import { spacing } from "styleguide/spacing";

const value = spacing.inset("xl");
```

## Inset helpers
In addition to the normal inset function, there is a couple of extra helper functions for solving common problems:

### Squish
Used to create paddings where the side padding is slighly larger than the top & bottom padding.

```jsx
import { spacing } from "styleguide/spacing";

const value = spacing.insetSquish("xl");
```

### Stretch
Used to create paddings where the side padding is slighly smaller than the top & bottom padding.

```jsx
import { spacing } from "styleguide/spacing";

const value = spacing.insetStretch("xl");
```

## Border
```jsx
import { border } from "styleguide/border";

const value = border("thick");
```



# Color

Not yet implemented.